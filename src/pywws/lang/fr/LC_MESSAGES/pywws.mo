��    7      �              �     �     �     �     �     �     �     �     �     �  #   �          /     J     W     s     �     �     �     �     �     �     �     �     �               9     ;     >     B     F     I     V     o     �     �     �  	   �     �     �          ,     A     C     G     K     S     c     r     �     �     �     �     �  �  �     q     �  "   �     �     �     �     �     �     �  +   �     	     =	  
   ]	  $   h	     �	     �	     �	     �	     �	     �	     �	  -   �	  (   �	  )   
     F
  #   ]
     �
     �
     �
     �
     �
     �
  $   �
     �
  $   �
          ,  
   D  !   O  +   q     �  "   �     �     �     �  	   �     �            	   +     5     F     V     m   Becoming fine Changeable, mending Changeable, some rain Date Day E ENE ESE Fairly fine, improving Fairly fine, possible showers early Fairly fine, showers likely Fairly fine, showery later Fine weather Fine, becoming less settled Fine, possible showers Mostly very unsettled N NE NNE NNW NW Occasional rain, worsening Rain at frequent intervals Rain at times, very unsettled Rain, very unsettled Rather unsettled clearing later S SE SSE SSW SW Settled fine Showery early, improving Showery, becoming less settled Showery, bright intervals Stormy, may improve Stormy, much rain Time (%Z) Unsettled, probably improving Unsettled, rain later Unsettled, short fine intervals Unsettled, some rain W WNW WSW falling falling quickly falling slowly falling very rapidly rising rising quickly rising slowly rising very rapidly steady Project-Id-Version:  pywws
Report-Msgid-Bugs-To: "jim@jim-easterbrook.me.uk"
POT-Creation-Date: 2013-09-04 18:48-0400
PO-Revision-Date: 2011-06-26 12:20+0000
Last-Translator: Jacques Desroches <jacques.desroches@meteoduquebec.com>
Language: fr
Language-Team: French
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.11.0
 en amélioration Variable, en amélioration Variable, quelques précipitations Date Jour E ENE ESE Plutôt beau, en amélioration Plutôt beau, averses possibles en matinée Plutôt beau, averse possible Plutôt beau, averse en soirée Beau temps Beau temps, tendance à se dégrader Beau, averses possibles Très perturbé N NE NNE NNO NO Précipitations occasionnelles, se dégradant Precipitations à intervalles fréquents Quelques précipitations, très perturbé Pluie, très perturbé plutôt variable, en amélioration. S SE SSE SSO SO Beau temps établi Averse en matinée, en amélioration Averses, temps se découvrant Averses éparses, belles éclaircies Tempête, pourrait s'améliorer Tempête, fortes pluies Heure (%Z) Perturbé, amélioration probable Variable, quelques précipitations tardives Perturbé, de rares éclairces Variable, quelques précipitations O ONO OSO en baisse en baisse rapide en baisse lente en baisse très rapide en hausse en hausse rapide en hausse lente en hausse très rapide stable 