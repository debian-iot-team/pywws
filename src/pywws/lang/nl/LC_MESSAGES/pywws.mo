��    7      �              �     �     �     �     �     �     �     �     �     �  #   �          /     J     W     s     �     �     �     �     �     �     �     �     �               9     ;     >     B     F     I     V     o     �     �     �  	   �     �     �          ,     A     C     G     K     S     c     r     �     �     �     �     �  �  �     M     ]  %   |     �     �     �     �     �     �  %   �  %   �     	  	   ;	  
   E	     P	     j	     ~	     �	     �	     �	     �	     �	     �	  $   �	     �	  !   
     '
     )
     ,
     0
     4
     7
      I
     j
     �
     �
     �
  	   �
      �
  !        &     E     a     c     g     k  
   q     |     �     �  
   �     �     �     �   Becoming fine Changeable, mending Changeable, some rain Date Day E ENE ESE Fairly fine, improving Fairly fine, possible showers early Fairly fine, showers likely Fairly fine, showery later Fine weather Fine, becoming less settled Fine, possible showers Mostly very unsettled N NE NNE NNW NW Occasional rain, worsening Rain at frequent intervals Rain at times, very unsettled Rain, very unsettled Rather unsettled clearing later S SE SSE SSW SW Settled fine Showery early, improving Showery, becoming less settled Showery, bright intervals Stormy, may improve Stormy, much rain Time (%Z) Unsettled, probably improving Unsettled, rain later Unsettled, short fine intervals Unsettled, some rain W WNW WSW falling falling quickly falling slowly falling very rapidly rising rising quickly rising slowly rising very rapidly steady Project-Id-Version:  pywws
Report-Msgid-Bugs-To: jim@jim-easterbrook.me.uk
POT-Creation-Date: 2012-11-16 11:57+0000
PO-Revision-Date: 2010-11-27 21:56+0100
Last-Translator: root <rick@sulman.org>
Language: nl
Language-Team: Dutch
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=ascii
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.11.0
 Wordt mooi weer Wisselvallig, wordt beter weer Wisselvallig, kleine kans op neerslag Datum Dag O ONO OZO Redelijk weer, wordt beter Redelijk weer, mogelijk snel neerslag Redelijk weer, grote kans op neerslag Redelijk weer, later neerslag Mooi weer Mooi weer  Mooi weer, mogelijk buien Zeer onstabiel weer N NO NNO NNW NW Soms regen, wordt slechter weer Regelmatig neerslag Regelmatig neerslag, wordt onstabiel Neerslag, zeer onstabiel weer Onstabiel weer, later opklaringen Z ZO ZZO ZZW ZW Stabiel mooi weer Eerst neerslag, later beter weer Neerslag, wordt onstabiel Neerslag met opklaringen Stormachtig, wordt beter weer Stormachtig met veel neerslag Tijd (%Z) Onstabiel weer, later beter weer Onstabiel weer met later neerslag Onstabiel weer met opklaringen Onstabiel weer met neerslag W WNW WZW daalt daalt snel daalt langzaam daalt zeer snel stijgt daalt snel stijgt langzaam stijgt zeer snel stabiel weer 