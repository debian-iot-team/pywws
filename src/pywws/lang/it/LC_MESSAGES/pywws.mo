��    7      �              �     �     �     �     �     �     �     �     �     �  #   �          /     J     W     s     �     �     �     �     �     �     �     �     �               9     ;     >     B     F     I     V     o     �     �     �  	   �     �     �          ,     A     C     G     K     S     c     r     �     �     �     �     �  �  �     ^  	   o     y     �     �     �     �     �  "   �  -   �  "   �  !   	  	   A	     K	     c	     �	     �	     �	     �	     �	     �	  "   �	     �	  !   �	     
  '   
     C
     E
     H
     L
     P
     S
     e
     ~
     �
     �
     �
  
   �
  "   �
  .   !  &   P  '   w     �     �     �     �     �     �     �     �     �     
          2   Becoming fine Changeable, mending Changeable, some rain Date Day E ENE ESE Fairly fine, improving Fairly fine, possible showers early Fairly fine, showers likely Fairly fine, showery later Fine weather Fine, becoming less settled Fine, possible showers Mostly very unsettled N NE NNE NNW NW Occasional rain, worsening Rain at frequent intervals Rain at times, very unsettled Rain, very unsettled Rather unsettled clearing later S SE SSE SSW SW Settled fine Showery early, improving Showery, becoming less settled Showery, bright intervals Stormy, may improve Stormy, much rain Time (%Z) Unsettled, probably improving Unsettled, rain later Unsettled, short fine intervals Unsettled, some rain W WNW WSW falling falling quickly falling slowly falling very rapidly rising rising quickly rising slowly rising very rapidly steady Project-Id-Version: pywws 13.06
Report-Msgid-Bugs-To: "jim@jim-easterbrook.me.uk"
POT-Creation-Date: 2013-12-15 13:56+0100
PO-Revision-Date: 2013-09-15 09:38+0200
Last-Translator: Edoardo <edoardo69@hotmail.it>
Language: it
Language-Team: Italian
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=ascii
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.11.0
 In miglioramento Variabile Variabile, qualche rovescio Data Giorno E ENE ESE Abbastanza bello, in miglioramento Abbastanza bello, possibili rovesci mattutino Abbastanza bene, rovesci probabili Abbastanza bello, leggeri rovesci Bel tempo Bello, in peggioramento Bello, possibilita di rovesci Veramente molto instabile N NE NNE NNO NO Occasionale pioggia, peggioramento Frequenti rovesci Pioggia a tratti, molto instabile Pioggia, molto instabile Piuttosto instabile, migliora sul tardi S SE SSE SSO SO Bel tempo stabile Rovesci, in migliormanto Piovoso, in peggioramento Piovoso, schiarite a intervalli Temporalesco, in miglioramento Temporalesco, molta pioggia Tempo (%Z) Instabile, probabile miglioramento Alternanza di nuvole e rovesci a fine giornata Alternanza di nuvole e brevi schiarite Alternanza di nuvole e qualche rovescio O ONO OSO diminuzione diminuzione veloce diminuzione lenta diminuzione molto rapida aumento aumento veloce aumento lento aumento molto rapidamente stazionaria 