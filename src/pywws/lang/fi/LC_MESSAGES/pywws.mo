��    7      �              �     �     �     �     �     �     �     �     �     �  #   �          /     J     W     s     �     �     �     �     �     �     �     �     �               9     ;     >     B     F     I     V     o     �     �     �  	   �     �     �          ,     A     C     G     K     S     c     r     �     �     �     �     �  �  �     P     Y     e     �     �     �     �  
   �     �  +   �  +   �  (   )	  	   R	     \	  %   |	     �	     �	     �	     �	     �	     �	     �	     
  %   
     D
  '   `
     �
     �
     �
     �
     �
     �
     �
  "   �
  !        $     C     [  $   `      �  !   �     �     �     �     �     �               %     ?     F     V     f     �   Becoming fine Changeable, mending Changeable, some rain Date Day E ENE ESE Fairly fine, improving Fairly fine, possible showers early Fairly fine, showers likely Fairly fine, showery later Fine weather Fine, becoming less settled Fine, possible showers Mostly very unsettled N NE NNE NNW NW Occasional rain, worsening Rain at frequent intervals Rain at times, very unsettled Rain, very unsettled Rather unsettled clearing later S SE SSE SSW SW Settled fine Showery early, improving Showery, becoming less settled Showery, bright intervals Stormy, may improve Stormy, much rain Time (%Z) Unsettled, probably improving Unsettled, rain later Unsettled, short fine intervals Unsettled, some rain W WNW WSW falling falling quickly falling slowly falling very rapidly rising rising quickly rising slowly rising very rapidly steady Project-Id-Version:  pywws
Report-Msgid-Bugs-To: jim@jim-easterbrook.me.uk
POT-Creation-Date: 2012-11-16 11:57+0000
PO-Revision-Date: 2012-11-16 05:02+0200
Last-Translator: root <karte2@gmail.com>
Language: fi
Language-Team: Finnish
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.11.0
 Selkenee Vaihtelevaa Vaihtelevaa, vähän sadetta Päivämäärä Päivä Itä Itäkoilinen Itäkaakko Melko selkeää, selkenee Melko selkeää, mahdollisesti kuurosateita Melko selkeää, mahdollisesti kuurosateita Melko selkeää, myöhemmin kuurosateita Selkeää Selkeää, muuttuu epävakaaksi Selkeää, mahdollisesti kuurosateita Enimmäkseen hyvin epävakaista P Koilinen Pohjoiskoilinen Pohjoisluode Luode Ajoittain sadetta, huononee Toistuvaa sadetta Ajoittain sadetta, hyvin epävakaista Sadetta, hyvin epävakaista Melko epävakaista, myöhemmin selkenee Etelä Kaakko Eteläkaakko Etelälounas Lounas Pysyy selkeänä Kuurosateita, selkenee Kuurosateita, tulossa epävakaaksi Kuurosateita, ajoittain selkeää Myrsky, mahdollisesti selkenee Myrsky, paljon sadetta  Aika Epävakaista, mahdollisesti selkenee Epävakaista, myöhemmin sateita Epävakaista, ajoittain selkeää Epävakaista, sadetta Länsi Länsiluode Länsilounas Laskee Laskee nopeasti Laskee hitaasti Laskee erittäin nopeasti Nousee Nousee nopeasti Nousee hitaasti Nousee erittäin nopeasti Vakaa 