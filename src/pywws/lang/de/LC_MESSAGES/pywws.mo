��    7      �              �     �     �     �     �     �     �     �     �     �  #   �          /     J     W     s     �     �     �     �     �     �     �     �     �               9     ;     >     B     F     I     V     o     �     �     �  	   �     �     �          ,     A     C     G     K     S     c     r     �     �     �     �     �  �  �     [     z  $   �     �     �     �     �     �  ,   �  0   �  :   &	  -   a	     �	  /   �	  "   �	     �	     
     
     
     
     
  9   
  0   V
  !   �
  !   �
  )   �
     �
     �
     �
     �
            0      (   Q  '   z     �     �  	   �  *   �  2     0   G  %   x     �     �     �     �     �     �     �     �     �     �          &   Becoming fine Changeable, mending Changeable, some rain Date Day E ENE ESE Fairly fine, improving Fairly fine, possible showers early Fairly fine, showers likely Fairly fine, showery later Fine weather Fine, becoming less settled Fine, possible showers Mostly very unsettled N NE NNE NNW NW Occasional rain, worsening Rain at frequent intervals Rain at times, very unsettled Rain, very unsettled Rather unsettled clearing later S SE SSE SSW SW Settled fine Showery early, improving Showery, becoming less settled Showery, bright intervals Stormy, may improve Stormy, much rain Time (%Z) Unsettled, probably improving Unsettled, rain later Unsettled, short fine intervals Unsettled, some rain W WNW WSW falling falling quickly falling slowly falling very rapidly rising rising quickly rising slowly rising very rapidly steady Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: jim@jim-easterbrook.me.uk
POT-Creation-Date: 2013-05-18 22:06+0200
PO-Revision-Date: 2010-04-11 20:00+0200
Last-Translator: Johabu <johabu96@yahoo.de>
Language: de
Language-Team: German
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.11.0
 Entwicklung zu schönem Wetter Wechselhaftes Wetter Wechselhaftes Wetter mit etwas Regen Datum Tag O ONO OSO Recht schönes Wetter, Besserung zu erwarten Recht schönes Wetter, anfangs eventuell Schauer Recht schönes Wetter, wahrscheinlich teilweise regnerisch Recht schönes Wetter, später Niederschläge Schönes Wetter Schönes Wetter, wird allerdings unbeständiger Schönes Wetter, eventuell Schauer Sehr wechselhaftes Wetter N NO NNO NNW NW Gelegentlich Niederschläge, Verschlechterung des Wetters Hin und wieder Regen, sehr unbeständiges Wetter Zuweilen Regen, sehr unbeständig Regen, sehr unbeständiges Wetter Unbeständiges Wetter, später Aufklarung S SO SSO SSW SW Beständig schönes Wetter Anfangs noch Schauer, dann Besserung des Wetters Regnerisches Wetter, wird unbeständiger Regnerisches Wetter mit heiteren Phasen Stürmig, eventuell Besserung Stürmig mit viel Niederschlag Zeit (%Z) Unbeständiges Wetter, eventuell Besserung Unbeständiges Wetter, später auch Niederschläge Unbeständiges Wetter mit kurzen heiteren Phasen Unbeständiges Wetter mit etwas Regen W WNW WSW fallend schnell fallend langsam fallend sehr schnell fallend steigend schnell steigend langsam steigend sehr schnell steigend gleichbleibend 