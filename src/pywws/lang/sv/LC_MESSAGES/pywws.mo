��    7      �              �     �     �     �     �     �     �     �     �     �  #   �          /     J     W     s     �     �     �     �     �     �     �     �     �               9     ;     >     B     F     I     V     o     �     �     �  	   �     �     �          ,     A     C     G     K     S     c     r     �     �     �     �     �  �  �     �     �     �     �     �     �     �     	     	  "   "	     E	     b	     {	     �	     �	     �	     �	     �	     �	     �	     �	  $   �	     
      
     ?
  $   U
     z
     |
     
     �
     �
     �
  %   �
     �
     �
     �
          -  &   6  $   ]     �     �     �     �     �     �     �     �     �          
          ,     C   Becoming fine Changeable, mending Changeable, some rain Date Day E ENE ESE Fairly fine, improving Fairly fine, possible showers early Fairly fine, showers likely Fairly fine, showery later Fine weather Fine, becoming less settled Fine, possible showers Mostly very unsettled N NE NNE NNW NW Occasional rain, worsening Rain at frequent intervals Rain at times, very unsettled Rain, very unsettled Rather unsettled clearing later S SE SSE SSW SW Settled fine Showery early, improving Showery, becoming less settled Showery, bright intervals Stormy, may improve Stormy, much rain Time (%Z) Unsettled, probably improving Unsettled, rain later Unsettled, short fine intervals Unsettled, some rain W WNW WSW falling falling quickly falling slowly falling very rapidly rising rising quickly rising slowly rising very rapidly steady Project-Id-Version:  pywws_14_05
Report-Msgid-Bugs-To: jim@jim-easterbrook.me.uk
POT-Creation-Date: 2014-05-05 17:27+0100
PO-Revision-Date: 2014-05-21 16:25+0000
Last-Translator: Sunshades <joacim@ahlstrand.info>
Language: sv
Language-Team: Swedish (http://www.transifex.com/projects/p/pywws_14_05/language/sv/)
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.11.0
 På väg mot vackert väder Varierande, mot förbättring Varierande, något regn Datum Dag O ONO OSO Halvklart, mot förbättring Halvklart, möjligen tidiga skurar Halvklart, troligtvis skurar Halvklart, senare skurar Vackert väder Vackert, på väg mot ostadigt Vackert, möjliga skurar Mestadels mycket ostadigt N NO NNV NNV NV Tillfälligt regn, mot sämre väder Mestadels regn Regnperioder , mycket ostadigt Regn, mycket ostadigt Något ostadigt, senare uppklarnande S SO SSO SSV SV Stadigt vackert väder Tidiga skurar, på väg mot bättring Skurar, mot mer ostadigt Skurar, tidvis uppklarnande Blåsigt, kan förbättras Blåsigt, mycket regn Tid (%Z) Ostadigt, troligtvis mot förbättring Ostadigt, senare övergående i regn Ostadigt, korta solchanser Ostadigt, något regn V VNV VSV fallande hastigt fallande långsamt fallande mycket hastigt fallande ökande hastigt ökande långsamt ökande mycket hastigt ökande stadigt 