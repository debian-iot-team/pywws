��    7      �              �     �     �     �     �     �     �     �     �     �  #   �          /     J     W     s     �     �     �     �     �     �     �     �     �               9     ;     >     B     F     I     V     o     �     �     �  	   �     �     �          ,     A     C     G     K     S     c     r     �     �     �     �     �  �  �  	   �     �  !   �     �     �     �     �     �     �  &   	  &   )	  $   P	     u	     {	     �	     �	     �	     �	     �	     �	     �	  '   �	     �	  $   
     5
     L
     g
     i
     l
     p
     t
     w
     �
     �
      �
     �
  !   �
  	        "      =  !   ^  "   �     �     �     �     �     �     �     �               &     :     S   Becoming fine Changeable, mending Changeable, some rain Date Day E ENE ESE Fairly fine, improving Fairly fine, possible showers early Fairly fine, showers likely Fairly fine, showery later Fine weather Fine, becoming less settled Fine, possible showers Mostly very unsettled N NE NNE NNW NW Occasional rain, worsening Rain at frequent intervals Rain at times, very unsettled Rain, very unsettled Rather unsettled clearing later S SE SSE SSW SW Settled fine Showery early, improving Showery, becoming less settled Showery, bright intervals Stormy, may improve Stormy, much rain Time (%Z) Unsettled, probably improving Unsettled, rain later Unsettled, short fine intervals Unsettled, some rain W WNW WSW falling falling quickly falling slowly falling very rapidly rising rising quickly rising slowly rising very rapidly steady Project-Id-Version:  pywws
Report-Msgid-Bugs-To: jim@jim-easterbrook.me.uk
POT-Creation-Date: 2016-02-25 10:44+0000
PO-Revision-Date: 2016-02-17 15:50+0000
Last-Translator: Pablo Vera <pablo.vera82@gmail.com>
Language: es
Language-Team: Spanish (http://www.transifex.com/jim-easterbrook/pywws/language/es/)
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.11.0
 Mejorando Variable Variable, algunas precipitaciones Fecha Dia E ENE ESE Moderadamente bueno, mejorando Moderadamente bueno, algunas lloviznas Moderadamente bueno, algunas lloviznas Moderadamente bueno, lloviznas luego Bueno Bueno, cambiando a inestable Bueno, posibles lloviznas Bastante inestable N NE NNE NNO NO Precipitaciones ocasionales, empeorando Lluvias frecuentes Chubascos ocasionales, muy inestable Lluvias, muy inestable Inestable, aclarando luego S SE SSE SSO SO Establecido Lloviznas, mejorando luego Lluvioso, tendencia a mejorar Lluvioso, aclarando a intervalos Tormentas, puede mejorar Tormentas, muchas precipitaciones Hora (%Z) Inestable, posible mejoria Inestable, precipitaciones luego Inestable con periodos de mejoria Inestable, algunas precipitaciones O ONO OSO descendiendo descendiendo rapidamente descendiendo lentamente descendiendo muy rapidamente subiendo subiendo rapidamente subiendo lentamente subiendo muy rapidamente estable 