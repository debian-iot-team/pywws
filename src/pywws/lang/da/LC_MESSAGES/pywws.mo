��    7      �              �     �     �     �     �     �     �     �     �     �  #   �          /     J     W     s     �     �     �     �     �     �     �     �     �               9     ;     >     B     F     I     V     o     �     �     �  	   �     �     �          ,     A     C     G     K     S     c     r     �     �     �     �     �  �  �     d     w     �     �     �     �     �     �     �  &   �     �     	  	   +	     5	     C	     \	     o	     q	     t	     x	     |	     	  
   �	  "   �	     �	  $   �	     
     
     
     
     
     
      
     ;
     K
     \
     w
     �
      �
     �
  +   �
     �
                         %     4     D     Y     b     q     �     �   Becoming fine Changeable, mending Changeable, some rain Date Day E ENE ESE Fairly fine, improving Fairly fine, possible showers early Fairly fine, showers likely Fairly fine, showery later Fine weather Fine, becoming less settled Fine, possible showers Mostly very unsettled N NE NNE NNW NW Occasional rain, worsening Rain at frequent intervals Rain at times, very unsettled Rain, very unsettled Rather unsettled clearing later S SE SSE SSW SW Settled fine Showery early, improving Showery, becoming less settled Showery, bright intervals Stormy, may improve Stormy, much rain Time (%Z) Unsettled, probably improving Unsettled, rain later Unsettled, short fine intervals Unsettled, some rain W WNW WSW falling falling quickly falling slowly falling very rapidly rising rising quickly rising slowly rising very rapidly steady Project-Id-Version:  pywws
Report-Msgid-Bugs-To: jim@jim-easterbrook.me.uk
POT-Creation-Date: 2012-11-16 11:57+0000
PO-Revision-Date: 2011-06-26 23:41+0100
Last-Translator: Kyle Gordon <kyle@lodge.glasgownet.com>
Language: da
Language-Team: Danish
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=iso-8859-1
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.11.0
 I bedring til Godt Foranderlig, I Bedring  Ustadigt, nogen regn Dato Dag O ONO OSO Nogenlunde, I bedring Nogenlunde, mulighed for byger tidligt Nogenlunde, Muligvis byger Nogenlunde, regnfuld senere Godt vejr Godt, uroligt Godt, mulighed for byger Meget Foranderligt N NO NNO NNV NV Nogen regn, Forvaerring Regn byger Til tider regn, meget foranderligt Regn, meget foranderligt Meget Foranderligt, opklaring senere S SO SSO SSV SV Roligt, Godt Regnfuld tidlig, I bedring Byger, Ustadigt Byger, nogen sol Bl�sende, mulig forbedring Bl�sende, Regnvejr klokken Foranderligt, muligvis opklaring Foranderligt, Regn senere Foranderligt,Korte intervaler med fint vejr Foranderligt, Nogen Regn V VNV VSV Faldende falder hurtigt Falder langsomt Falder meget hurtigt Stigende Stiger hurtigt Stiger langsomt Stiger meget hurtigt Stabil 